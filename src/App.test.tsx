import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';

test('renders get your random quote button', () => {
  render(<App />);
  const buttonElement = screen.getByText(/get your random quote/i);
  expect(buttonElement).toBeInTheDocument();
});
