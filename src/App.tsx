import React from 'react';
import { useState, useEffect } from 'react';
import Header from './components/Header/Header';
import Footer from './components/Footer/Footer';
import Grid from '@mui/material/Grid';
import RandomSearch from './components/RandomSearch/RandomSearch';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import AboutPage from './components/AboutPage/AboutPage';
import Form from './components/Form/Form';
import Search from './components/PersonalizedSearch/PersonalisedSearch';
import axios from 'axios';
import Container from '@mui/material/Container';


const App: React.FC = () => {
	const [ clicked, setClicked ] = useState(false);
	const [ quote, setQuote ] = useState('');
	const [ author, setAuthor ] = useState('');
	const [ isFetching, setIsFetching ] = useState(true);

	const [tag, setTag] = useState("");
	const [authorSearch, setAuthorSearch] = useState("");

	useEffect(
		() => {
			if (clicked) {
				axios.get(`https://api.quotable.io/random`).then((data) => {
					console.log(data.data);
					setQuote(data.data.content);
					setAuthor(data.data.author);
					setClicked(false);
					setIsFetching(false);
				});
			}
		},
		[ clicked ],
	);

	return (
		<div className="App">
			<BrowserRouter>
			<Container maxWidth="lg">
				<Grid container spacing={13} sx={{ mt: 10 }}>
					<Grid item xs={12}>
						<Header />
					</Grid>
				

					<Routes>
						<Route
							path="/"
							element={
								<>
								<Grid item xs={12} textAlign="center">
									<RandomSearch setClicked={setClicked} isFetching={isFetching} quote={quote} author={author}/>
								</Grid>
								<Grid item xs={12} textAlign="center">
								<Form setTag={setTag} setAuthorSearch={setAuthorSearch}/>
							</Grid>
							<Grid item xs={12}>
								<Search tag={tag} authorSearch={authorSearch}/>
							</Grid>
							</>
							}
						/>

						<Route
							path="/about"
							element={
								<Grid item xs={12}   textAlign="center">
									<AboutPage />
								</Grid>
							}
						/>
					</Routes>
				
					<Grid item xs={12} sx={{ marginLeft:"auto", marginRight:"auto", paddingBottom:2}} >
						<Footer />
					</Grid>
				</Grid>
				</Container>
			</BrowserRouter>
		</div>
	);
};

export default App;
