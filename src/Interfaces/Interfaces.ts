export interface funcProps {
  quote:string;
  author:string;
  setClicked:(clicked:boolean)=>void;
  isFetching:boolean
}


export interface authorsRecords {
  name:string;
  description:string;
  bio:string;
  link:string;
  dateAdded:string;
  dateModified:string;
  quoteCount:number;
  slug:string;
  id:string
}

export interface formSearchFunc{

  setTag:(tag:string)=>void;
  setAuthorSearch:(AuthorSearch:string)=>void;
  
}
export interface formSearchData{
  authorSearch:string;
  tag:string;
  
}

export interface quoteRecordData{
author: string;
authorSlug: string;
content: string;
dateAdded: string;
dateModified: string;
length: number;
tags: [];
_id: string
}

