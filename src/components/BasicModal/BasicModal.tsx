import * as React from 'react';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import axios from 'axios';
import { useState, useEffect } from 'react';
import Link from '@mui/material/Link';
import { authorsRecords } from '../../Interfaces/Interfaces';

const style = {
	position: 'absolute' as 'absolute',
	top: '50%',
	left: '50%',
	transform: 'translate(-50%, -50%)',
	width: 400,
	bgcolor: 'background.paper',
	border: '1px solid #000',
	boxShadow: 20,
	p: 5,
};

const BasicModal: React.FC<{ author: string }> = (props) => {
	const [ open, setOpen ] = React.useState(false);

	const handleClose = () => setOpen(false);

	const [ isFetching, setIsFetching ] = useState(true);

	const [ authorInfo, setAuthorInfo ] = useState<authorsRecords>({
		name: '',
		description: '',
		bio: '',
		link: '',
		dateAdded: '',
		dateModified: '',
		quoteCount: 0,
		slug: '',
		id: '',
	});

	const [ clickedMore, setClickedMore ] = useState(false);

	useEffect(
		() => {
			if (clickedMore) {
				axios.get(`https://api.quotable.io/search/authors?query=${props.author}`).then((data) => {
					setAuthorInfo(data.data.results[0]);
					setClickedMore(false);
					setIsFetching(false);
				});
			}
		},
		[ props.author, clickedMore ],
	);
	const clickHandler: React.MouseEventHandler<HTMLButtonElement> = (e) => {
		if (e.currentTarget.textContent === 'Learn More About The Author') {
			setClickedMore(true);
			setOpen(true);
		}
	};

	return (
		<div>
			<Button sx={{ textAlign: 'center' }} onClick={clickHandler}>
				Learn More About The Author
			</Button>

			<Modal
				open={open}
				onClose={handleClose}
				aria-labelledby="modal-modal-title"
				aria-describedby="modal-modal-description"
			>
				<Box sx={style}>
					<Typography id="modal-modal-title" variant="h6" component="h2">
						{!isFetching && authorInfo.name}
					</Typography>
					<Typography id="modal-modal-description" sx={{ mt: 2 }}>
						{!isFetching && authorInfo.description}
					</Typography>
					<Typography id="modal-modal-content" sx={{ mt: 2 }}>
						{!isFetching && authorInfo.bio}
					</Typography>
					<Link href={authorInfo.link}>{!isFetching && authorInfo.link}</Link>
				</Box>
			</Modal>
		</div>
	);
};
export default BasicModal;
