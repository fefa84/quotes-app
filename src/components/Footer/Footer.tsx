import * as React from 'react';
import Box from '@mui/material/Box';
import BottomNavigation from '@mui/material/BottomNavigation';
import BottomNavigationAction from '@mui/material/BottomNavigationAction';
import HomeRoundedIcon from '@mui/icons-material/HomeRounded';
import CircleRoundedIcon from '@mui/icons-material/CircleRounded';
import { Link } from 'react-router-dom';

const Footer: React.FC = () => {
	const [ value, setValue ] = React.useState(0);

	return (
		<Box sx={{ textAlign: 'center'}}>
			<BottomNavigation
				showLabels
				value={value}
				onChange={(event, newValue) => {
					setValue(newValue);
				}}
			>
				<BottomNavigationAction component={Link} to="/" label="Search" icon={<HomeRoundedIcon />} />

				<BottomNavigationAction component={Link} to="/about" label="About us" icon={<CircleRoundedIcon />} />
			</BottomNavigation>
		</Box>
	);
};

export default Footer;
