import * as React from 'react';
import Button from '@mui/material/Button/Button';
import Grid from '@mui/material/Grid';
import TextField from '@mui/material/TextField/TextField';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import { useForm } from 'react-hook-form';
import { formSearchFunc } from '../../Interfaces/Interfaces';


const Form: React.FC<formSearchFunc> = (props) => {
	const { register, handleSubmit, formState: { errors } } = useForm();

	const formHandler = (data: any) => {
		props.setTag(data.tag);
		props.setAuthorSearch(data.authorSearch);
	};

	return (
		<Box onSubmit={handleSubmit(formHandler)} textAlign="center" component="form" noValidate autoComplete="off">
			<Grid container spacing={2}>
				<Grid item xs={12}>
					<Typography variant="h5" mb={3} align="center">
						OR choose the quote according to your needs
					</Typography>
				</Grid>
				<Grid item xs={12}>


					<Box sx={{
						display: 'flex',
						justifyContent:"center"
					}}>


					<TextField
						sx={{ mr: 2 }}
						id="authorSearch"
						type="text"
						{...register('authorSearch')}
						label="Author"
						helperText="You have to write a full name ex.Albert Einstein"
						variant="outlined"
					/>

					<TextField sx={{ mr: 2 }} value="Search by author OR by Tag">
						disabled
					</TextField>

					<TextField
						id="tag"
						type="text"
						{...register('tag')}
						label="Tag"
						helperText="ex.technology"
						variant="outlined"
					/>
					</Box>
				</Grid>
				<Grid item xs={12}>
					<Button type="submit" size="large">Search</Button>
				</Grid>
			</Grid>
		</Box>
	);
};

export default Form;
