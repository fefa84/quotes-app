import React from 'react';
import logo from './logo.png';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography/Typography';
import AppBar from '@mui/material/AppBar/AppBar';
import Grid from '@mui/material/Grid';
import Fade from '@mui/material/Fade/Fade';

const Header: React.FC = () => {
	return (
		<Fade in={true} timeout={2000}>
			<AppBar sx={{display:"flex", justifyContent:"space-between", alignContent:"center"}}>
				<Box p={3}>
					<Grid container spacing={2}>
						<Grid item xs={3}>
							<Box
								component="img"
								sx={{
									maxHeight: { xs: 100, s:120,  md: 140 },
									maxWidth: { xs: 100, s:120, md: 140 },
									borderRadius: 50,
								}}
								alt="logo"
								src={logo}
							/>
						</Grid>
						<Grid item xs={9}>
							<Typography variant="h1" sx={{ fontSize:{ xs: 30, s: 45, md:50 }}}>
								Choose your quote of a day
							</Typography>
							<Typography variant="body1" sx={{ fontSize:{ xs: 25, s:35, md:40 } }}>
								to live you life better
							</Typography>
						</Grid>
					</Grid>
				</Box>
			</AppBar>
		</Fade>
	);
};

export default Header;
