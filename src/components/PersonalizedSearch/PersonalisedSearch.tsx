import React from 'react';
import { useState, useEffect } from 'react';
import { formSearchData } from '../../Interfaces/Interfaces';
import axios from 'axios';
import Fade from '@mui/material/Fade/Fade';
import TableContainer from '@mui/material/TableContainer/TableContainer';
import Card from '@mui/material/Card/Card';
import TableResults from '../TableResults/TableResults';

const Search: React.FC<formSearchData> = (props) => {
	const [ quotesList, setQuotesList ] = useState([]);
	const [ isFetching, setIsFetching ] = useState(true);

	useEffect(
		() => {
			if (props.tag) {
				axios.get(`https://quotable.io/quotes?tags=${props.tag}`).then((data) => {
					console.log(data.data.results);
					setQuotesList(data.data.results);
					setIsFetching(false);
				});
			}
			if (props.authorSearch) {
				axios.get(`https://quotable.io/quotes?author=${props.authorSearch}`).then((data) => {
					console.log(data.data.results);
					setQuotesList(data.data.results);
					setIsFetching(false);
				});
			}
		},
		[ props.tag, props.authorSearch ],
	);
	return (
		<Fade in={true} timeout={2000}>
			<TableContainer component={Card}>
				{quotesList && !isFetching && <TableResults quotesList={quotesList} />}
			</TableContainer>
		</Fade>
	);
};

export default Search;
