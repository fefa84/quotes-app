import React from 'react';
import Card from '@mui/material/Card/Card';
import Typography from '@mui/material/Typography/Typography';
import CardContent from '@mui/material/CardContent';
import BasicModal from '../BasicModal/BasicModal';

const QuoteCard: React.FC<{ quote: string; author: string }> = (props) => {
	return (
		<Card sx={{ minWidth: 275, boxShadow: 3 }}>
			<CardContent>
				<Typography sx={{ fontSize: 16 }} color="text.secondary" gutterBottom>
					Quote of the Day
				</Typography>
				<Typography variant="body2">{props.quote}</Typography>
				<Typography sx={{ mb: 1.5 }} color="text.secondary">
					{props.author}
				</Typography>
			</CardContent>

			<BasicModal author={props.author} />
		</Card>
	);
};

export default QuoteCard;
