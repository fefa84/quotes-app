import React from 'react';
import Button from '@mui/material/Button/Button';
import { funcProps } from '../../Interfaces/Interfaces';
import QuoteCard from '../QuoteCard/QuoteCard';
import Grid from '@mui/material/Grid';

const RandomSearch: React.FC<funcProps> = (props) => {
	const clickHandler: React.MouseEventHandler<HTMLButtonElement> = (e) => {
		if (e.currentTarget.textContent === 'Get your random quote') {
			props.setClicked(true);
		}
	};

	return (
		<Grid container spacing={5}>
			{!props.isFetching && (
				<Grid item xs={12}>
					<QuoteCard quote={props.quote} author={props.author} />
				</Grid>
			)}
			<Grid item xs={12}>
				<Button onClick={clickHandler} variant="contained">
					Get your random quote
				</Button>
			</Grid>
		</Grid>
	);
};

export default RandomSearch;
