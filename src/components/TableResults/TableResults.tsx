import React from 'react';
import TableRowResult from './TableRowResult';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody/TableBody';
import TableRow from '@mui/material/TableRow/TableRow';
import TableCell from '@mui/material/TableCell/TableCell';
import TableHead from '@mui/material/TableHead/TableHead';
import { quoteRecordData } from '../../Interfaces/Interfaces';

const TableResults: React.FC<{ quotesList: quoteRecordData[] }> = (props) => {
	//repoList-pełna lista obiektów repozytoriów
	return (
		<Table sx={{ minWidth: 650 }} aria-label="table">
			<TableHead>
				<TableRow>
					<TableCell align="center">Quote</TableCell>
					<TableCell align="center">Author</TableCell>
				</TableRow>
			</TableHead>
			<TableBody>
				{props.quotesList.map((el: quoteRecordData) => {
					return <TableRowResult key={el._id} content={el.content} author={el.author} />;
				})}
			</TableBody>
		</Table>
	);
};

export default TableResults;
