import TableRow from '@mui/material/TableRow/TableRow';
import TableCell from '@mui/material/TableCell/TableCell';

const TableRowResult: React.FC<{ key: string; content: string; author: string }> = (props) => {
	return (
		<TableRow>
			<TableCell align="left">{props.content}</TableCell>
			<TableCell align="right">{props.author}</TableCell>
		</TableRow>
	);
};

export default TableRowResult;
